export const ACTION_LOAD_MORE_COMMENTS = 'comments/loadMore'

export const ACTION_VOTE = 'comments/vote'

export const ACTION_SHOW_REPLY = 'comments/showReply'

export const ACTION_REPLY = 'comments/reply'
