import { VOTE_DOWN, VOTE_NONE, VOTE_UP } from '../constants/commentVotes'
import { GROUP_PRO } from '../constants/userGroups'

export default {
    comments: {
        1: {
            id: 1,
            userId: 1,
            text: 'Some text',
            parentId: null,
            date: 'Apr 24, 2011',
        },
        2: {
            id: 2,
            userId: 2,
            text: 'Some response',
            parentId: 1,
            date: 'Apr 24, 2012',
        },
        3: {
            id: 3,
            userId: 1,
            text: 'Some response for response',
            parentId: 3,
            date: 'Sep 15, 2018',
        },
    },
    commentsIds: [1],
    users: {
        1: {
            id: 1,
            name: 'log1st',
            avatar: 'http://some.com/image.jpg',
            group: GROUP_PRO,
            iq: 10,
        },
        2: {
            id: 2,
            name: 'sergey',
            avatar: 'http://some.com/photo.jpg',
            group: null,
            iq: 12,
        },
    },
    votes: {
        1: {
            rating: 30,
            voteDirection: VOTE_UP,
        },
        2: {
            rating: 0,
            voteDirection: VOTE_NONE,
        },
        3: {
            rating: 12,
            voteDirection: VOTE_DOWN,
        },
    },
    replyId: null,
}
