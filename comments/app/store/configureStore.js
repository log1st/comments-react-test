import { applyMiddleware, compose, createStore } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from '../reducers/reducers'

export default initialState => createStore(
    rootReducer,
    initialState,
    compose(
        applyMiddleware(thunk),
    ),
)
