import {
    ACTION_LOAD_MORE_COMMENTS, ACTION_REPLY,
    ACTION_SHOW_REPLY,
    ACTION_VOTE,
} from '../constants/commentsActionsNames'
import { addReply } from '../services/commentsService'

export const loadMoreComments = () => ({
    type: ACTION_LOAD_MORE_COMMENTS,
})

export const vote = () => ({
    type: ACTION_VOTE,
})

export const showReply = () => ({
    type: ACTION_SHOW_REPLY,
})

export const reply = () => (dispatch) => {
    addReply()
        .then((comment) => {
            dispatch({
                type: ACTION_REPLY,
                payload: comment,
            })
        })
}
