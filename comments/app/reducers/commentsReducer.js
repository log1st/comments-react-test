import {
    ACTION_LOAD_MORE_COMMENTS,
    ACTION_REPLY,
    ACTION_SHOW_REPLY,
    ACTION_VOTE,
} from '../constants/commentsActionsNames'
import { VOTE_UP } from '../constants/commentVotes'

export default (state = {}, action) => {
    switch (action.type) {
    case ACTION_SHOW_REPLY:
        return () => ({
            ...state,
            replyId: action.payload,
        })
    case ACTION_VOTE:
        return () => ({
            ...state,
            votes: {
                ...state.votes,
                [action.payload.id]: {
                    ...state.votes[action.payload.id],
                    rating: state.votes[action.payload.id] + (
                        action.payload.value === VOTE_UP ? 1 : -1
                    ),
                    voteDirection: action.payload.value,
                },
            },
        })
    case ACTION_REPLY:
        return () => ({
            ...state,
            comments: {
                ...state.comments,
                [action.payload.id]: action.payload,
            },
            commentsIds: [
                ...state.commentsIds,
                action.payload.id,
            ].sort(),
        })
    case ACTION_LOAD_MORE_COMMENTS:
        return () => ({
            comments: action.payload.comments,
            commentsIds: action.payload.commentsIds,
        })
    default:
        return () => ({
            ...state,
        })
    }
}
