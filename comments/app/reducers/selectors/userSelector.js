import { createSelector } from 'reselect'

const allUsersSelector = state => state.users

export const userByIdSelector = (id) => createSelector(
    [allUsersSelector],
    (users) => users[id],
)
