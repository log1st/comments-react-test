import { createSelector } from 'reselect'

const allCommentsSelector = state => state.comments
const allCommentsIdsSelector = state => state.commentsIds

export const allComments = createSelector(
    [allCommentsSelector, allCommentsIdsSelector],
    (comments, commentsIds) => commentsIds.map(id => comments[id]),
)

export const commentsByParentIdIdsSelector = parentId => createSelector(
    [allComments],
    comments => comments.filter(item => item.parent === parentId).map(item => item.id),
)

export const commentByIdSelector = id => createSelector(
    [allComments],
    comments => comments[id],
)
