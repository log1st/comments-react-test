import React from 'react'
import { compose, withProps } from 'recompose'
import { connect } from 'react-redux'
import CommentItemComponent from '../../../core/components/CommentItemComponent'
import CommentsThreadContainer from './CommentsThreadContainer'
import { commentByIdSelector, commentsByParentIdIdsSelector } from '../../reducers/selectors/commentsSelectors'
import { showReply, vote } from '../../actions/commentsActions'
import UserContainer from './UserContainer'

export default compose(
    connect(
        (state, { commentId }) => ({
            comment: commentByIdSelector(commentId)(state),
            commentsIds: commentsByParentIdIdsSelector(commentId)(state),
            rating: state.votes[commentId].rating,
            voteDirection: state.votes[commentId].direction,
        }),
        {
            onVoteClick: vote,
            onShowReplyClick: showReply,
        },
    ),
    withProps({
        renderUser: UserContainer,
    }),
)(
    ({
        comment,
        commentsIds,
        renderComment,
        renderUser,
        onVoteClick,
        onShowReplyClick,
        rating,
        voteDirection,
    }) => (
        <CommentItemComponent
            onShowReplyClick={onShowReplyClick}
            onVoteClick={onVoteClick}
            comment={comment}
            renderUser={renderUser}
            rating={rating}
            voteDirection={voteDirection}
        >
            <CommentsThreadContainer
                commentsIds={commentsIds}
                renderComment={renderComment}
            />
        </CommentItemComponent>
    ),
)
