import { withProps } from 'recompose'
import { connect } from 'react-redux'
import CommentItemComponent from '../../../core/components/CommentItemComponent'
import { userByIdSelector } from '../../reducers/selectors/userSelector'

export default connect(
    (state, { userId }) => ({
        user: userByIdSelector(userId)(state),
    }),
    withProps({
        renderUser: CommentItemComponent.User,
    }),
)(
    ({ renderUser, user }) => renderUser(user),
)
