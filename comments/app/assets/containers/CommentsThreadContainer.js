import React from 'react'
import { compose, withProps } from 'recompose'
import CommentsThreadComponent from '../../../core/components/CommentsThreadComponent'
import CommentItemContainer from './CommentItemContainer'

export default compose(
    withProps(
        () => ({
            renderComment: CommentItemContainer,
        }),
    ),
)(
    ({
        commentsIds,
        renderComment,
    }) => (
        <CommentsThreadComponent
            commentsIds={commentsIds}
            renderComment={renderComment}
        />
    ),
)
