import React from 'react'
import { compose, withProps } from 'recompose'
import { connect } from 'react-redux'
import { commentsByParentIdIdsSelector } from '../../reducers/selectors/commentsSelectors'
import CommentsLayout from '../../../core/assets/layouts/comments/CommentsLayout'
import { loadMoreComments } from '../../actions/commentsActions'
import CommentsThreadContainer from "./CommentsThreadContainer";

export default compose(
    connect(
        state => ({
            commentsIds: commentsByParentIdIdsSelector(null)(state),
        }),
        {
            onLoadMoreClick: loadMoreComments,
        },
    ),
    withProps({
        loadMoreText: 'Load all',
    }),
)(
    ({
        commentsIds,
        onLoadMoreClick,
        loadMoreText,
    }) => (
        <CommentsLayout
            loadMoreText={loadMoreText}
            onLoadMoreClick={onLoadMoreClick}
        >
            <CommentsThreadContainer commentsIds={commentsIds} />
        </CommentsLayout>
    ),
)
