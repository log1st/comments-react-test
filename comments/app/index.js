import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'
import initialState from './store/initialState'

const store = configureStore(initialState)

ReactDOM.render(
    <Provider store={store} />,
)
