export const addReply = (parentId, text) => new Promise((resolve) => resolve({
    id: Math.round(Math.random() * 100000),
    text,
    parentId,
    userId: 1,
    date: 'now',
}))
