import React from 'react'
import ButtonElement from '../../elements/button/ButtonElement'

const LoadMoreButton = ({ onClick, text }) => (<ButtonElement
    onClick={onClick}
    text={text}
/>)

export default ({
    children,
    onLoadMoreClick,
    loadMoreText,
}) => (
    <div>
        {children}
        <LoadMoreButton onClick={onLoadMoreClick} text={loadMoreText} />
    </div>
)
