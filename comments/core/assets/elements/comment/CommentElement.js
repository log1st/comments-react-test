import React from 'react'
import ButtonElement from '../button/ButtonElement'
import { VOTE_DOWN, VOTE_UP } from '../../../../app/constants/commentVotes'

export default ({
    userId,
    text,
    date,
    renderUser,
    onVoteClick,
    voteDirection,
    rating,
}) => (
    <div>
        {renderUser(userId)}
        {text}
        {date}
        <div>
            <ButtonElement
                isActive={voteDirection === VOTE_UP}
                onClick={() => onVoteClick(VOTE_UP)}
            />
            {rating}
            <ButtonElement
                isActive={voteDirection === VOTE_DOWN}
                onClick={() => onVoteClick(VOTE_DOWN)}
            />
        </div>
    </div>
)
