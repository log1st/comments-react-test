import React from 'react'
import userGroups from '../../../../app/constants/userGroups'

export default ({
    name,
    avatar,
    group,
    iq,
}) => (
    <div>
        <img alt={name} src={avatar} />
        {name}
        <span>{userGroups[group] || `group${group}`}</span>
        <span>IQ: {iq}</span>
    </div>
)
