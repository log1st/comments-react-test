import React from 'react'

const CommentsThreadComponent = ({
    commentsIds,
    renderComment,
}) => (
    <div>
        {commentsIds.map(id => renderComment({
            commentId: id,
            renderComment,
            key: id,
        }))}
    </div>
)


export default CommentsThreadComponent
