import React from 'react'
import CommentElement from '../../elements/comment/CommentElement'
import UserElement from '../../elements/user/UserElement'

const CommentItemComponent = ({
    comment,
    onShowReplyClick,
    onVoteClick,
    children,
    renderUser,
    rating,
    voteDirection,
}) => (
    <div>
        <CommentElement
            onShowReplyClick={onShowReplyClick}
            onVoteClick={onVoteClick}
            {...comment}
            renderUser={renderUser}
            rating={rating}
            voteDirection={voteDirection}
        />
        {children}
    </div>
)

CommentItemComponent.User = UserElement

export default CommentItemComponent
